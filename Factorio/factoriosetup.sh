#!/bin/bash
# A centos build for factorio 
# Uses AWS ami-fedafc9d
# Run as sudo 

# to Run game:
# su factorio
# ./factorio-server-manager --dir /opt/factorio &
# Browse to http://<serverIP>:8080
# Copy created saves to /opt/factorio/save
# or /opt/factorio/bin/x64/factorio --create /opt/factorio/saves/my-save.zip 
# To gen a manual save with custom settings: 
# su factorio
# cd /opt/factorio
# factorio --create saves/jhnewsave2.zip --map-gen-settings path-to-file-with-desired-map-generation-settings.json

####################
# Vars and links   #
####################

installfac ()
{

##############
# Mkdirs	 #
##############

mkdir /opt/staging


#################################
# Get Dependencies and packages #
#################################

sleep 10s
yum -y install tmux wget git vim




#########################
# Setup factorio server #
#########################

# Get factorio
cd /opt

wget https://www.factorio.com/get-download/0.14.9/headless/linux64

# untar factorio

tar -xzvf linux64

cd /opt/factorio

cat /home/centos/josh.map-gen-settings.json > /opt/factorio/data/map-gen-settings.json

# run factorio install 


useradd factorio

chown -R factorio:factorio /opt/factorio
}

getmanager ()
{
	# Gets the factorio server manager  and sets it up.

	yum -y install unzip
	cd /opt
	wget https://github.com/MajorMJR/factorio-server-manager/releases/download/0.4.3/factorio-server-manager-linux-x64.zip
	unzip factorio-server-manager-linux-x64.zip
	chown -R factorio:factorio /opt/factorio-server
}


if [[ $1 == install ]]; then 
	installfac
fi

if [[ $1 == console ]]; then
	getmanager
fi



