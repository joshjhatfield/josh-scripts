#!/bin/bash
# A simple puppet setup script to make my life easier in AWS, take the qwhole git folder and run this script from within it. 
# built for ami-ba3e14d9 ubuntu 14.04 generic image.
# run dos2unix over this script if it causes issues. 
# Example
# ./serversetup.sh puppetmaster - sets up and configures a puppetmaster server from vanilla image.

#variables here

IP=`ifconfig eth0 | grep "inet addr" | awk '{print $2}' | cut -c 6-`
H=`hostname -f`
PM=$H


# functions here 

puppetmaster ()
{
	apt-get update
	apt-get -y install puppetmaster-passenger
	service apache2 stop
	apt-get -y install awscli
	apt-get -y install htop
	sleep 10s
	rm -rf /var/lib/puppet/ssl
	sleep 2s
	cat ./josh.puppet.conf > /etc/puppet/puppet.conf
	cat ./josh.puppetmaster.conf > /etc/apache2/sites-available/puppetmaster.conf
	sed -i "s/FQDNVAR1/$H/g" /etc/puppet/puppet.conf
	sed -i "s/FQDNVAR2/$PM/g" ./josh.agent.puppet.conf
	sleep 1s
	rm -f /var/run/puppet/master.pid
	puppet master --verbose
	sleep 30s
	PMPID=`ps -ef | grep "puppet master --verbose" | head -n1 | awk '{print $2}'`
	kill -9 $PMPID	
	rm -f /var/run/puppet/master.pid
	service apache2 start
	sleep 5s
	touch /etc/puppet/manifests/site.pp
	puppet cert list -all
}


puppetagent()
{
	apt-get update
	apt-get -y install puppet
	apt-get -y install awscli
	sleep 5s
	cat ./josh.agent.puppet.conf > /etc/puppet/puppet.conf
	touch /etc/default/puppet
	echo "START=yes" > /etc/default/puppet
	puppet agent --enable
	service puppet restart
	sleep 10s
	puppet agent --test
	puppet agent -tv --noop
}


# Pos variable conditions

if [[ $1 = puppetmaster ]]; then 
	puppetmaster
fi

if [[ $1 = puppetagent ]]; then 
	puppetagent
fi
