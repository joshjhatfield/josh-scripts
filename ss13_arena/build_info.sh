#!/bin/bash

# Setup stuff for the arena server on ubuntu, untested

# Run as root



setup () {
	# Get the files
	cd /opt
	sudo apt-get update
	wget http://www.byond.com/download/build/511/511.1370_byond_linux.zip
	sudo apt-get -y install make htop git unzip build-essential
	sudo dpkg --add-architecture i386
	sudo apt-get update
	sudo apt-get -y install lib32z1 lib32ncurses5 libmysqlclient-dev:i386
	unzip 511.1370_byond_linux.zip
	cd /opt/byond/
	sudo make install
}


setup_ss13() {
	# Get Paradise
	cd /opt
	git clone https://github.com/ParadiseSS13/Paradise.git
	cd /opt/Paradise
	DreamMaker paradise.dme
	
	
}


turn_on_server() {
	sudo DreamDaemon /opt/Paradise/paradise.dmb 3333 -trusted
}


case $1 in
	byondsetup )
	setup
		;;
	paradise )
	setup_ss13
	;;
	
esac
