#!/bin/bash

# Startup and run script for ss13
# reccomended crontab
#1 */3 * * * /opt/ss13arena.sh restart


GAMEPATH=/opt/Paradise/paradise.dmb
PORT=3333
BYONDPID=`ps -C DreamDaemon | grep DreamDaemon | awk '{print $1}'`

byond_check() {

    if [ -z "$(ps -C DreamDaemon | grep DreamDaemon)" ]
    then
		# /usr/local/bin/DreamDaemon $GAMEPATH $PORT -invisible -trusted &
        /usr/local/bin/DreamDaemon $GAMEPATH $PORT -trusted &
    fi

}


byond_kill(){
	kill -9 $BYONDPID
}



case $1 in
start)
	byond_check
		;;
check)
	byond_check
		;;
stop)
	byond_kill
		;;
restart)
	byond_kill
	sleep 5s
	byond_check
		;;
*)
	printf "\n\nUsage options are ss13arena.sh start | stop | check | restart  I suggest making a softlink to your PATH\n\n"

esac